/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "CEENBoT_DS1820.h"
/**
 CEENBoT_DS1820_get will get the temperature from the DS1820 sensor on an 1-Wire bus. This is dependant on the CEENBoT OW library to be installed or one that works just like it.

 Example:
 ...
 int tempC = CEENBoT_TEMP_get();

 LCD_printf("%d degrees celsius", tempC);
 ...
 */
int CEENBoT_DS1820_get() {
	int rval = -1;
	uint8_t temperature[2];

	//Reset, skip ROM and start temperature conversion
	OW_reset();
	OW_write_byte(OW_CMD_SKIPROM);
	OW_write_byte(OW_CMD_CONVERTTEMP);

	//Wait until conversion is complete
	while (!OW_read_bit())
		;

	//Reset, skip ROM and send command to read Scratchpad
	OW_reset();
	OW_write_byte(OW_CMD_SKIPROM);
	OW_write_byte(OW_CMD_RSCRATCHPAD);

	//Read Scratchpad (only 2 first bytes)
	temperature[0] = OW_read_byte();
	temperature[1] = OW_read_byte();
	OW_reset();

	//Store temperature integer digits
	rval = temperature[0] >> 4;
	rval |= (temperature[1] & 0x7) << 4;

	return rval;
}
